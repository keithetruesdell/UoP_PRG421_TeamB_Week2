package TrackLaneAssignment;
/**
 *------------------------------------------------------------------------------
 * @title       Learning Team - Program Improvement 1
 * @week        Week 2
 * @author      Learning Team B - Cedrick A. Drafton, Henry Hernandez, Keith Truesdell
 * @school      University of Phoenix - Online
 * @course      PRG/421 - Java Programming II
 * @instructor  Edward Spear
 * @duedate     11/07/2016
 *------------------------------------------------------------------------------
 * ** WEEK 1 INDIVIDUAL ASSIGNMENT ** 
 * Write a Java program (non-GUI preferred) to demonstrate the use of an ArralyList.
 * 
 * The program should allow a user to do the following:
 *      Add, edit, delete different types of animals
 *      Select an animal, and the corresponding characteristics will be displayed (such as color, vertebrate or invertebrate, can swim, etc.)
 *      The program must use ArrayList(s) to work with these animal objects.
 * 
 * Include a brief documentation (in the code or in a separate document)
 * to explain the input (if any), processing and output of the program
 *------------------------------------------------------------------------------ 
 * ** SOURCE CODE ORIGINALLY CREATED BY ** 
 * Cedrick A. Drafton 
 * PRG/421 Java Programming II
 * Week 1- Singleton Pattern Program
 *
 * This Singleton Pattern Program will give you permission to assign an athlete to  
 * the track in one of the eight lanes, one sprinter will be assigned 
 * at a time 
 *------------------------------------------------------------------------------ 
 * ** WEEK 2 TEAM ASSIGNMENT ** 
 * Select one program submitted by a team member in Week One.
 * 
 * Suggest at least 3 ways to improve the selected program.  
 * One of these improvements must be related to applying assertion, 
 *  exceptions, data formatting or localization.
 * 
 * Write code and test the improved program.  Deliverables should include:
 *      The source code file(s) of the improved program.
 *      A 2- to 3-page paper on the approach the team has taken to improve the program.
 *      Why some of the changes are relevant to this week's objectives.
 *      Any challenges the team encountered and suggest any future improvements.
 * ------------------------------------------------------------------------------ 
 */

// imports librariies for java program to use
import java.util.*;

public class TrackLaneAssignment_rev1_20161031 {
    
    // runner names
    private String runner[] = new String[8]; 
    
    // to store the single instance
    //called the singleton
    private static TrackLaneAssignment_rev1_20161031 uniqueInstance;
    
    // Exists only to ensure that no callers can instantiate the object directly
    private TrackLaneAssignment_rev1_20161031() { }
    
    //This public static method will return a reference to the instance
    //this function is here so that the can be read
    //A private constructor, is here to ensure that cannot be 
    //instantiated directly
    public static TrackLaneAssignment_rev1_20161031 getInstance() {
        if (uniqueInstance == null) {
            uniqueInstance = new TrackLaneAssignment_rev1_20161031();
        }
        //return the only running instance....new or existing
        return uniqueInstance;
    }
    
    private static Scanner input; 
    
    //This will assign a sprinter to one of the lanes on the track
    public void assignRunner(String name, int lane) {
        runner[lane] = name;
        System.out.println(runner[lane] + " you are assigned to the track.  Good Luck!!");
    }
    
    //Display the runner
    public void displayRunner(int lane) {
        System.out.println("Sprinter: " + runner[lane]);
    }
    
    //the main method called when the application is run
    public static void main(String[] args) {
        String name;
        input = new Scanner(System.in);
                
        //Sprinter will be assigned to Lane 1
        System.out.print("Sprinter for Lane 1: ");
        name = input.nextLine();
        
        // get instance of the Singleton class TrackLaneAssignment
        TrackLaneAssignment_rev1_20161031 lane1 = TrackLaneAssignment_rev1_20161031.getInstance();
        
        // A sprinter will be assigned to lane 1
        lane1.assignRunner(name, 1);
        
        //Sprinter will be assigned to Lane 2
        System.out.print("Sprinter for Lane 2: ");
        name = input.nextLine();
        
        // get instance of the Singleton class TrackLaneAssignment
        TrackLaneAssignment_rev1_20161031 lane2 = TrackLaneAssignment_rev1_20161031.getInstance();
        
        //A sprinter will be assigned to lane 2
        lane2.assignRunner(name, 2);
        
        //Sprinter will be assigned to Lane 3
        System.out.print("Sprinter for Lane 3: ");
        name = input.nextLine();
        
        // get instance of the Singleton class TrackLaneAssignment
        TrackLaneAssignment_rev1_20161031 lane3 = TrackLaneAssignment_rev1_20161031.getInstance();
        
        //A sprinter will be assigned to lane 3
        lane3.assignRunner(name, 3);
        
        // Sprinter will be assigned to Lane 4
        System.out.print("Sprinter for Lane 4: ");
        name = input.nextLine();
        
        // get instance of the Singleton class TrackLaneAssignment
        TrackLaneAssignment_rev1_20161031 lane4 = TrackLaneAssignment_rev1_20161031.getInstance();
        
        // A sprinter will be assigned to lane 4
        lane4.assignRunner(name, 4);
        
        //Sprinter will be assigned to Lane 5
        System.out.print("Sprinter for Lane 5: ");
        name = input.nextLine();
        
        // get instance of the Singleton class TrackLaneAssignment
        TrackLaneAssignment_rev1_20161031 lane5 = TrackLaneAssignment_rev1_20161031.getInstance();
        
        // A sprinter will be assigned to lane 5
        lane5.assignRunner(name, 5);
        
        //Sprinter will be assigned to lane 6
        System.out.print("Sprinter for Lane 6: ");
        name = input.nextLine();
        
        // get instance of the Singleton class TrackLaneAssignment
        TrackLaneAssignment_rev1_20161031 lane6 = TrackLaneAssignment_rev1_20161031.getInstance();
        
        // assign the runner to the lane 6
        lane6.assignRunner(name, 6);
        
        //Sprinter will be assigned to Lane 7
        System.out.print("Sprinter for Lane 7: ");
        name = input.nextLine();
        
        // get instance of the Singleton class TrackLaneAssignment
        TrackLaneAssignment_rev1_20161031 lane7 = TrackLaneAssignment_rev1_20161031.getInstance();
        
        //A sprinter will be assigned to lane 7
        lane7.assignRunner(name, 7);
        
        //Sprinter will be assigned to lane 8
        System.out.print("Sprinter for Lane 8: ");
        name = input.nextLine();
        
        // get instance of the Singleton class TrackLaneAssignment
        TrackLaneAssignment_rev1_20161031 lane8 = TrackLaneAssignment_rev1_20161031.getInstance();
        
        // A sprinter will be assigned to lane 8
        lane8.assignRunner(name, 8);
        
        // Display the sprinters name for each lane assignment.
        System.out.print("\n\n");
        lane1.displayRunner(1);
        lane2.displayRunner(2);
        lane3.displayRunner(3);
        lane4.displayRunner(4);
        lane5.displayRunner(5);
        lane6.displayRunner(6);
        lane7.displayRunner(7);
        lane8.displayRunner(8);	
    }
}