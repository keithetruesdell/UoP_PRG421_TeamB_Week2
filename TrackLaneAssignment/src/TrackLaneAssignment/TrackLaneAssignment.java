package TrackLaneAssignment;
/**
 *------------------------------------------------------------------------------
 * @title       Learning Team - Program Improvement 1
 * @week        Week 2
 * @author      Learning Team B - Cedrick A. Drafton, Henry Hernandez, Keith Truesdell
 * @school      University of Phoenix - Online
 * @course      PRG/421 - Java Programming II
 * @instructor  Edward Spear
 * @duedate     11/07/2016
 *------------------------------------------------------------------------------
 * ** WEEK 1 INDIVIDUAL ASSIGNMENT ** 
 * Write a Java program (non-GUI preferred) to demonstrate the use of an ArralyList.
 * 
 * The program should allow a user to do the following:
 *      Add, edit, delete different types of animals
 *      Select an animal, and the corresponding characteristics will be displayed (such as color, vertebrate or invertebrate, can swim, etc.)
 *      The program must use ArrayList(s) to work with these animal objects.
 * 
 * Include a brief documentation (in the code or in a separate document)
 * to explain the input (if any), processing and output of the program
 *------------------------------------------------------------------------------ 
 * ** SOURCE CODE ORIGINALLY CREATED BY ** 
 * Cedrick A. Drafton 
 * PRG/421 Java Programming II
 * Week 1- Singleton Pattern Program
 *
 * This Singleton Pattern Program will give you permission to assign an athlete to  
 * the track in one of the eight lanes, one sprinter will be assigned 
 * at a time 
 *------------------------------------------------------------------------------ 
 * ** WEEK 2 TEAM ASSIGNMENT ** 
 * Select one program submitted by a team member in Week One.
 * 
 * Suggest at least 3 ways to improve the selected program.  
 * One of these improvements must be related to applying assertion, 
 *  exceptions, data formatting or localization.
 * 
 * Write code and test the improved program.  Deliverables should include:
 *      The source code file(s) of the improved program.
 *      A 2- to 3-page paper on the approach the team has taken to improve the program.
 *      Why some of the changes are relevant to this week's objectives.
 *      Any challenges the team encountered and suggest any future improvements.
 * ------------------------------------------------------------------------------
 * ** IMPROVEMENT SUGGSTIONS FROM TEAM B **
 *      1. parameters for the displayRunner method with a for loop to clean up code
 *      2. assignRunner method must validate user input using an exception
 *      3. have the runner names and lanes as an array within the singleton class for a single reference
 * ------------------------------------------------------------------------------
 */

// imports librariies for java program to use
import java.util.*;

public class TrackLaneAssignment {
    
    // #3 suggestion for improvement
    // runner names
    private final String runner[] = new String[9]; 
    
    // to store the single instance
    //called the singleton
    private static TrackLaneAssignment uniqueInstance;
    
    // Exists only to ensure that no callers can instantiate the object directly
    private TrackLaneAssignment() { }
    
    //This public static method will return a reference to the instance
    //this function is here so that the can be read
    //A private constructor, is here to ensure that cannot be 
    //instantiated directly
    public static TrackLaneAssignment getInstance() {
        if (uniqueInstance == null) {
            uniqueInstance = new TrackLaneAssignment();
        }
        //return the only running instance....new or existing
        return uniqueInstance;
    }
    
    //temporary variable for the user input
    private static Scanner input; 
    
    // #1 suggestion for improvment
    //This will assign a sprinter to one of the lanes on the track
    public void assignRunner(String name, int lane) {
        runner[lane] = name;
        System.out.println(runner[lane] + ", you are assigned to the track in lane " + lane + ".  Good Luck!!");
    }
    
    // #1 suggestion for improvment
    //Display the runner and the lane they are in
    public void displayRunner(int lane) {
        System.out.println("Sprinter Lane " + lane + ": " + runner[lane]);
    }
    
    //the main method called when the application is run
    public static void main(String[] args) throws emptyInputException {
        String name;
        input = new Scanner(System.in);
        
        // get instance of the Singleton class TrackLaneAssignment
        TrackLaneAssignment lanes = TrackLaneAssignment.getInstance();
        
        // #1 suggestion for improvment
        //loop through assigning a runner to each lane (item in array)
        for(int l=1; l<9; l++){
            //Sprinter will be assigned to Lane 1
            System.out.print("Sprinter for Lane " + l + ": ");
            name = input.nextLine();
            
            //throw exception for no input from user or if its all spaces
            if (name.trim().isEmpty()) {
                throw new emptyInputException();
            }
            
            // A sprinter will be assigned to lane 1
            lanes.assignRunner(name, l);
            
        }
        
        
        
        // Display the sprinters name for each lane assignment.
        // place a seperator row between input and the display
        System.out.print("\n ---------------- \n\n");
        
        // #1 suggestion for improvment
        //loop through the array of names to display the runners
        for(int l=1; l<9; l++) {
            lanes.displayRunner(l);
        }
        
        
        //expception methods
        class emptyInputException {
            
        }
    }

    private static class emptyInputException extends Exception {

        public emptyInputException() {
            System.out.println("No user input given for th is lane assignemtn");
        }
    }
}